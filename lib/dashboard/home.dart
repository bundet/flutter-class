import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_class/dashboard/todo_list.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.only(top: 100),
            child: Text(
              "Welcome to MyCodeLab",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black,
                fontSize: 30,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(10),
            child: Text(
              "Dunia sedang membutuhkan superhero seperti Anda!",
              style: TextStyle(
                fontWeight: FontWeight.normal,
                color: Colors.grey,
                fontSize: 15,
              ),
            ),
          ),
          Image.asset("assets/images/intro_image.png"),
          Padding(
            padding: EdgeInsets.all(50),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  primary: Colors.purple,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12)))),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ToDoList()));
              },
              child: Text("OPEN"),
            ),
          ),
        ],
      ),
    );
  }
}
